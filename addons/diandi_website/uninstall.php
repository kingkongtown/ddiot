<?php
/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2022-07-18 09:49:23
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2022-08-01 09:57:54
 */

namespace addons\diandi_website;

use common\components\addons\AddonsUninstall;

/**
 * 卸载.
 *
 * Class UnInstall
 */
class UnInstall extends AddonsUninstall
{
    public $addons = 'diandi_website';
}
