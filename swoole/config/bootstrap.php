<?php
/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2022-09-01 16:16:07
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2023-01-30 14:13:09
 */
Yii::setAlias('@Simps', dirname(__DIR__) . '/vendor/simps/mqtt/src');
Yii::setAlias('@ddswoole', dirname(dirname(__DIR__)) . '/swoole');
