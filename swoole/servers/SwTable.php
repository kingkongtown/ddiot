<?php
/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2022-08-26 14:29:24
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2022-09-15 07:28:06
 */

namespace ddswoole\servers;

use ddswoole\traits\InteractsWithSwooleTable;

 class SwTable
 {
     use InteractsWithSwooleTable;
 }
