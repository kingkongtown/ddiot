<?php
/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2022-08-17 09:25:45
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2022-08-23 09:47:53
 */

namespace ddswoole\components\tcp;

use diandi\swoole\server\BaseServer;
use diandi\swoole\server\UdpServer as ServerUdpServer;

class UdpServer extends ServerUdpServer
{
    
}
